<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BeerGarden
 */

?>
	</div><!-- #content -->
	
	<!-- Footer -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		
		<nav id="footer">
			<div class="row">
				<div class="card col-xs-6 col-sm-4 col-md-3 col-lg-3">
					<h4>Permalinks</h4>
					<ul class="nobullets">
						<?php output_menu_items('primary'); ?>
					</ul>
				</div>
				<div class="card col-xs-6 col-sm-4 col-md-3 col-lg-3">
					<h4>Shop</h4>
					<ul class="nobullets">
						<?php output_menu_items('shop'); ?>
					</ul>
				</div>
				<div class="card col-xs-6 col-sm-4 col-md-3 col-lg-3">
					<h4>User</h4>
					<ul class="nobullets">
						<?php output_menu_items('user'); ?>
					</ul>
				</div>
				<div class="card col-xs-6 col-sm-4 col-md-3 col-lg-3">
					<h4>Social Media</h4>
					<ul class="nobullets">
						<?php output_menu_items('social'); ?>
					</ul>
				</div>
			</div>
			
			<!-- Copyright Info -->
			<div class="row">
				<div class="col-xs-12 col-sm-12 site-info">
					<center>
						Website Designed and Built by <a href="http://lukejkw.bitbucket.org">Luke Warren Dev s3409172</a>
					</center>
				</div><!-- .site-info -->
			</div>
		</nav>
	
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>

<script src="<?php bloginfo('template_directory')?>/js/jquery-1.12.3.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_directory')?>/js/bootstrap.min.js" type="text/javascript"></script>

<script type="text/javascript">
	$(function(){
		// Add logo item
		$("<li class='logo'><a id='logo' href='<?php home_url( '/' )?>'><img src='<?php bloginfo('template_directory')?>/img/beer-garden-logo-small.png' alt='Beer Garden Logo'></a></li>")
			.insertAfter('#primary-menu :nth-child(2)')
	});	
</script>	
</html>
