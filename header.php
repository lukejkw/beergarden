<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package BeerGarden
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<!-- Include Stylesheets -->
<link rel="stylesheet" href="<?php bloginfo('template_directory')?>/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_directory')?>/css/bootstrap.min.css">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'beergarden' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<nav id="site-navigation" class="main-navigation navbar-collapse" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu', 'menu_class'=>'nav navbar-nav' ) ); ?>
		</nav><!-- #site-navigation -->
		
		<nav id="subnav" class="clearfix">
            <?php
			
			// Build left side of sub nav
			$locations = get_nav_menu_locations();
			$menu = wp_get_nav_menu_object( $locations[ 'submenu' ] );
			$menuitems = wp_get_nav_menu_items( $menu->term_id);
				
			if (is_array($menuitems) || is_object($menuitems)) {
				foreach ($menuitems as $item) {
					echo "<div class='subItem'>".
							"<a href='" . $item->url ."'>"
							."<i class='fa fa-beer' aria-hidden='true'></i>" . 
							$item->title .
							"</a>". 
						"</li>";
				}
			}
			
			// Build right side of sub nav with shop link
			$menu = wp_get_nav_menu_object( $locations[ 'shop' ] );
			$menuitems = wp_get_nav_menu_items( $menu->term_id);
			
			if (is_array($menuitems) || is_object($menuitems)) {
				foreach ($menuitems as $item) {
					echo "<div class='right'>".
							"<a href='" . $item->url ."'>".
							$item->title .
							"</a>". 
						"</li>";
				}
			}
			
			?>
        </nav>
		<?php
			if ( is_front_page()) : ?>
				<!-- Banner Image -->
				<div id="banner" class="">
					<img src="<?php bloginfo('template_directory')?>/img/beer_banner.jpg" class="img-responsive" alt="Beer garden banner image">
					<div id="banner-text">
						<h1>
							Beer Garden
						</h1>
						<h2>
							<?php
							$description = get_bloginfo( 'description', 'display' );
							if ( $description || is_customize_preview() ){
								echo $description;
							}
							?>
						</h2>
					</div>
				</div>
			<?php else : ?>
				<!-- Banner Image -->
				<div id="banner" class="">
					<img src="<?php bloginfo('template_directory')?>/img/shop_banner.jpg" class="img-responsive" alt="Beer garden banner image">
					<div id="banner-text">
						<h1>
							<?php echo wp_title(''); ?>
						</h1>
					</div>
				</div>
			<?php endif; ?>
		
		
	</header><!-- #masthead -->
	<style type="text/css">
	<?php 
		// Add margin to top if user logged in
		if(is_user_logged_in()){
			echo "#subnav, .page, nav.navbar-collapse div { margin-top: 30px; }";
		}
	?>	
	</style>
	<div id="content" class="site-content">
