#Beer Garden Web Mockup#

This code serves as the gneral vanilla HTML mockup of the beer garden wp theme.

We will be playing with styles and giving the general look and feel for the website

##References##

1. BootstrapJS from getbootstrap.com
2. FontAwesome from https://fortawesome.github.io/Font-Awesome/
3. Rockwell Font - http://ufonts.com/download/rockwell/32583.html

##Other Informarion##

This website was done for Assignment 2 Study Period 1 for the Curtin University Unit Internet Dynamic Delivery Design

RMIT ID: s3409172

##Contributions##

Feel free to fork this humble repo but not pull requests will be accepted.   